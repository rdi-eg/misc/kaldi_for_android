#!/usr/bin/env bash

if [[ "$1" != /* ]]; then
	echo "ndk must be an absolute path"
	exit 1
fi

if [ -z "$2" ]; then
	echo "You must specify the desired architecture in the second argument"
	echo "Valid values: 'armv7', 'armv8'"
	exit 1
fi

if [[ "$2" == "armv7" ]]; then
	patch="sawsan.patch"
    abi="armeabi-v7a"
elif [[ "$2" == "armv8" ]]; then
	patch="sawsan64.patch"
    abi="arm64-v8a"
else
	echo "You must specify the desired architecture in the second argument"
	echo "Valid values: 'armv7', 'armv8'"
	exit 1
fi

if [[ ! -d android_libs ]]; then
    git clone https://github.com/simonlynen/android_libs.git
    cd android_libs
    git am < ../${patch}
    cd ..
fi

cd android_libs/lapack

NDK=$1
# build for android
$NDK/ndk-build clean
$NDK/ndk-build

cd ../..

cp android_libs/lapack/obj/local/${abi}/*.a OpenBLAS/install/lib
