# Kaldi for Android

This repo will help you cross-compile kaldi for android as easily as possible.

All you need to do is:

- clone it.
- `cd` into it.
- run the scripts one after another (order is important)

Notes:

- You will need the Android NDK (version r21e). https://developer.android.com/ndk/downloads
- Extract the NDK somewhere reasonable.
- The scripts will expect the path to the NDK (The one you extracted in the previous step) as the first argument.
- The scripts will clone the needed repos if they're not found.
- The scripts build against the Android 23 ABI. Which is the minimum for kaldi to work with. I found this the hard way.
- The scripts will compile for both armv7 and armv8
- The scripts use the ARM soft float ABI. The hard float ABI is supposed to be faster but all the guides use soft and it seemed like a rabbit hole I wasn't ready to explore. Maybe at some point we'll switch to hard.

## Blis

Script: `./build_blis.sh /path/to/your/NDK [armv7-armv8]`

The second argument must either be 'armv7; or 'armv8' depending on which architecture you want to target.

## OpenBLAS

Script: `./build_openblas.sh /path/to/your/NDK [armv7-armv8]`

The second argument must either be 'armv7; or 'armv8' depending on which architecture you want to target.

We only do this step because kaldi uses openBLAS libraries to generate its binaries during the kaldi build.


## CLapack

Script: `./build_clapack.sh /path/to/your/NDK [armv7-armv8]`

The second argument must either be 'armv7; or 'armv8' depending on which architecture you want to target.

The scripts applies sawsan.patch or sawsan64.patch. These patches was made by trial and error and lots of frustration.

## OpenFST

Script: `./build_openfst.sh /path/to/your/NDK [armv7-armv8]`

The second argument must either be 'armv7; or 'armv8' depending on which architecture you want to target.

## kaldi

Script: `./build_kaldi.sh /path/to/your/NDK [armv7-armv8]`

The second argument must either be 'armv7; or 'armv8' depending on which architecture you want to target.

This one involved a lot of hacks and workarounds because the kaldi configure script had several issues with cross-compiling to android.

I filed the issues and did my best to document the script. Here they are, hopefully at the time you read this, these issues will be closed and you won't need most of the hacks in the script above:

- https://github.com/kaldi-asr/kaldi/issues/4123
- https://github.com/kaldi-asr/kaldi/issues/4138
- https://github.com/kaldi-asr/kaldi/issues/4137

## We're all done

Now you should have the kaldi libraries and its dependencies compiled for Android 23 ABI armv7.

* Blis should be in  `blis/lib/[arm32-arm64]/`
* CLapack should be in `android_libs/lapack/obj/local/[armeabi-v7a-arm64-v8a]/`
* OpenFST should be in `kaldi/tools/openfst/lib/`
* kaldi libs should be in `kaldi/lib/Release/`

## Thanks

- The amazing, helpful and responsive kaldi team
- Whoever contributed the support for cross compiling to Android to kaldi
- The people at openFST
- This Awesome blog post: https://jcsilva.github.io/2017/03/18/compile-kaldi-android/
- The guys at Blis
- My homie Simon Lynen for making this: https://github.com/simonlynen/android_libs.git
