#!/usr/bin/env bash

set -e

if [ -z "$1" ]; then
	echo "You must specify the install directory name in the first argument"
    echo "Examples: bin-armv7, bin-armv8"
	exit 1
fi

install_dir=$1

if [[ ! -d kaldi ]]; then
    echo "Can't find the kaldi directory"
    exit 1
fi

adb shell mkdir -p /data/local/tmp/$install_dir/

cd kaldi/src

for i in *; do
    if [[ $i == *"bin" ]]; then
        for j in $i/*; do
            if [[ $j != *"."* && $j != *"Makefile" ]]; then
                adb push $j /data/local/tmp/$install_dir/
            fi
        done
    fi
done

cd ..

for i in tools/openfst/bin/*; do
    adb push $i /data/local/tmp/$install_dir/
done