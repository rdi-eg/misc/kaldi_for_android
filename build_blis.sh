#!/usr/bin/env bash

android_min="23"

if [[ "$1" != /* ]]; then
	echo "ndk must be an absolute path"
	exit 1
fi

if [ -z "$2" ]; then
	echo "You must specify the desired architecture in the second argument"
	echo "Valid values: 'armv7', 'armv8'"
	exit 1
fi

if [[ "$OSTYPE" == "linux-gnu"* ]]; then
    os="linux-x86_64"
	proc_count="$(nproc)"
elif [[ "$OSTYPE" == "darwin"* ]]; then
    os="darwin-x86_64"
	proc_count=$(sysctl -n hw.logicalcpu)
else
	echo "Unsupported OS"
	exit 1
fi

if [[ "$2" == "armv7" ]]; then
	config=arm32
    echo "Currenly only armv8 is supported :("
    exit 1
elif [[ "$2" == "armv8" ]]; then
	config=arm64
else
	echo "You must specify the desired architecture in the second argument"
	echo "Valid values: 'armv7', 'armv8'"
	exit 1
fi

if [[ ! -d kaldi ]]; then
    git clone https://github.com/flame/blis.git
fi

export ndk=$1
export ndk_bin=$ndk/toolchains/llvm/prebuilt/${os}/bin/
export CC=$ndk_bin/aarch64-linux-android${android_min}-clang
export CXX=$ndk_bin/aarch64-linux-android${android_min}-clang++
export AR=$ndk_bin/aarch64-linux-android-ar
export RANLIB=$ndk_bin/aarch64-linux-android-ranlib
export CFLAGS=--sysroot=$ndk/sysroot

cd blis

make distclean

./configure --enable-cblas --disable-shared --disable-system ${config}

make -j ${proc_count}