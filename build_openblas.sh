#!/usr/bin/env bash

set -e

if [[ "$1" != /* ]]; then
	echo "ndk must be an absolute path"
	exit 1
fi

if [ -z "$2" ]; then
	echo "You must specify the desired architecture in the second argument"
	echo "Valid values: 'armv7', 'armv8'"
	exit 1
fi

android_min="23"

if [[ "$2" == "armv7" ]]; then
	toolchain="arm-linux-androideabi-4.9"
	triple="arm-linux-androideabi"
	target="ARMV7"
	arch="arch-arm"
elif [[ "$2" == "armv8" ]]; then
	toolchain="aarch64-linux-android-4.9"
	triple="aarch64-linux-android"
	target="CORTEXA57"
	arch="arch-arm64"
else
	echo "You must specify the desired architecture in the second argument"
	echo "Valid values: 'armv7', 'armv8'"
	exit 1
fi

if [[ "$OSTYPE" == "linux-gnu"* ]]; then
    os="linux-x86_64"
	proc_count="$(nproc)"
elif [[ "$OSTYPE" == "darwin"* ]]; then
    os="darwin-x86_64"
	proc_count=$(sysctl -n hw.logicalcpu)
else
	echo "Unsupported OS"
	exit 1
fi

if [[ ! -d OpenBLAS ]]; then
    git clone https://github.com/xianyi/OpenBLAS
fi

cd OpenBLAS

git checkout tags/v0.3.13

# clean previous builds
make clean
rm -rf install/

# Set path to ndk-bundle
export NDK_BUNDLE_DIR=$1

path_to_toolchain="${NDK_BUNDLE_DIR}/toolchains/llvm/prebuilt/${os}/bin"

# Set the PATH to contain paths to clang and arm-linux-androideabi-* utilities
export PATH="${path_to_toolchain}:$PATH"

# Set LDFLAGS so that the linker finds the appropriate libgcc
export LDFLAGS="-L${NDK_BUNDLE_DIR}/toolchains/${toolchain}/prebuilt/${os}/lib/gcc/${triple}/4.9.x"

# Set the clang cross compile flags
export CLANG_FLAGS="-fPIC -target ${triple} -marm -mfpu=vfp -mfloat-abi=softfp --sysroot ${NDK_BUNDLE_DIR}/platforms/android-${android_min}/${arch} -gcc-toolchain ${NDK_BUNDLE_DIR}/toolchains/${toolchain}/prebuilt/${os}/"

export CFLAGS=--sysroot=$NDK_BUNDLE_DIR/sysroot

#OpenBLAS Compile
make TARGET=${target} NO_SHARED=1 ONLY_CBLAS=1 AR=${path_to_toolchain}/${triple}-ar CC="$path_to_toolchain/clang ${CLANG_FLAGS}" HOSTCC=gcc -j${proc_count}

make install NO_SHARED=1 PREFIX=`pwd`/install  TARGET=${target} NO_SHARED=1 ONLY_CBLAS=1 AR=${path_to_toolchain}/${triple}-ar CC="$path_to_toolchain/clang ${CLANG_FLAGS}" HOSTCC=gcc -j${proc_count}

cp lapack-netlib/LAPACKE/include/*.h install/include