#!/usr/bin/env bash

set -e

if [[ "$1" != /* ]]; then
	echo "ndk must be an absolute path"
	exit 1
fi

if [ -z "$2" ]; then
	echo "You must specify the desired architecture in the second argument"
	echo "Valid values: 'armv7', 'armv8'"
	exit 1
fi

android_min="23"

if [[ "$2" == "armv7" ]]; then
	triple="arm-linux-androideabi"
	compiler="armv7a-linux-androideabi"
	FLAGS="-mfloat-abi=softfp"
elif [[ "$2" == "armv8" ]]; then
	triple="aarch64-linux-android"
	compiler="aarch64-linux-android"
else
	echo "You must specify the desired architecture in the second argument"
	echo "Valid values: 'armv7', 'armv8'"
	exit 1
fi

if [[ "$OSTYPE" == "linux-gnu"* ]]; then
    os="linux-x86_64"
	proc_count="$(nproc)"
elif [[ "$OSTYPE" == "darwin"* ]]; then
    os="darwin-x86_64"
	proc_count=$(sysctl -n hw.logicalcpu)
else
	echo "Unsupported OS"
	exit 1
fi

if [[ ! -d kaldi ]]; then
    git clone https://github.com/kaldi-asr/kaldi.git
	cd kaldi
	cd tools
	cp ../../config.patch ../../Makefile.patch .
	patch Makefile Makefile.patch
	cd ../..
fi

# Set path to ndk-bundle
export NDK_BUNDLE_DIR=$1

path_to_toolchain=${NDK_BUNDLE_DIR}/toolchains/llvm/prebuilt/${os}/bin

# Set the PATH to contain paths to clang and binutils
export PATH=${path_to_toolchain}:$PATH

cd kaldi/tools 

export AR=${path_to_toolchain}/${triple}-ar
export CXX=$path_to_toolchain/${compiler}${android_min}-clang++
export CXXFLAGS="-fPIC $FLAGS"
export CC=$path_to_toolchain/${compiler}${android_min}-clang
export CFLAGS="-fPIC $FLAGS"
export LDFLAGS="$FLAGS -static-libstdc++"
export LD=$TARGETARCH-ld
export OPENFST_CONFIGURE="--enable-static --enable-ngram-fsts --host=${triple}"

rm -rf openfst*

make openfst -j ${proc_count}
