#!/usr/bin/env bash

set -e

if [[ "$1" != /* ]]; then
	echo "ndk must be an absolute path"
	exit 1
fi

if [ -z "$2" ]; then
	echo "You must specify the desired architecture in the second argument"
	echo "Valid values: 'armv7', 'armv8'"
	exit 1
fi

android_min="23"

if [[ "$2" == "armv7" ]]; then
	triple="arm-linux-androideabi"
	compiler="armv7a-linux-androideabi"
elif [[ "$2" == "armv8" ]]; then
	triple="aarch64-linux-android"
	compiler="$triple"
else
	echo "You must specify the desired architecture in the second argument"
	echo "Valid values: 'armv7', 'armv8'"
	exit 1
fi

if [[ "$OSTYPE" == "linux-gnu"* ]]; then
    os="linux-x86_64"
	proc_count="$(nproc)"
elif [[ "$OSTYPE" == "darwin"* ]]; then
    os="darwin-x86_64"
	proc_count="$(sysctl -n hw.logicalcpu)"
	# the bsd sed requires an extra argument specifying a backup file
	# when using the -i option
	sed_bak=".bak"
else
	echo "Unsupported OS"
	exit 1
fi

echo "proc_count: " ${proc_count}

if [[ ! -d kaldi ]]; then
    git clone https://github.com/kaldi-asr/kaldi.git
fi

# Set path to ndk-bundle
export NDK_BUNDLE_DIR=$1

path_to_toolchain=${NDK_BUNDLE_DIR}/toolchains/llvm/prebuilt/${os}/bin

# Set the PATH to contain paths to clang and binutils
export PATH=${path_to_toolchain}:$PATH

if [[ "$2" == "armv7" ]]; then
	# a hack because the configure script expects a certain naming convention that was
	# deprecated by Android for sometime now
	# I filed an issue here: https://github.com/kaldi-asr/kaldi/issues/4123
	# As long as this issue is open, this hack is still needed
	rm -f $path_to_toolchain/${triple}-clang++
	echo "symlinking $path_to_toolchain/${compiler}${android_min}-clang++ to $path_to_toolchain/${triple}-clang++"
	ln -s $path_to_toolchain/${compiler}${android_min}-clang++ $path_to_toolchain/${triple}-clang++
elif [[ "$2" == "armv8" ]]; then
	echo "wtffffff"
	rm -f $path_to_toolchain/${triple}-clang++
	ln -s $path_to_toolchain/${triple}${android_min}-clang++ $path_to_toolchain/${triple}-clang++
fi

cd kaldi/src

export CXXFLAGS=-fPIC CXX=$path_to_toolchain/${compiler}${android_min}-clang++
export LDFLAGS=-static-libstdc++
./configure --static --use-cuda=no --android-incdir=${NDK_BUNDLE_DIR}/toolchains/llvm/prebuilt/${os}/sysroot/usr/include --host=${triple} --openblas-root=../../OpenBLAS/install --debug-level=0

# makes it a release build
# https://github.com/kaldi-asr/kaldi/issues/4138
sed -i${sed_bak} 's|-g # -O0||' kaldi.mk
sed -i${sed_bak} 's|-O1|-O3|' kaldi.mk

# disables the tests
# https://github.com/kaldi-asr/kaldi/issues/4137
sed -i${sed_bak} 's|TESTFILES|#TESTFILES|' base/Makefile
sed -i${sed_bak} 's|TESTFILES|#TESTFILES|' matrix/Makefile

make clean -j $proc_count

make depend -j $proc_count

make -j $proc_count
  
mkdir -p ../lib/Release/

for i in $(ls)
do
        output=$(echo $(ls $i | grep "\.a"))

        if [[ -n "$output" ]]; then
                cp $i/$output ../lib/Release/lib$output
        fi
done